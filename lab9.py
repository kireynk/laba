import csv
import random

x = []
y = []

for i in range(10):
    a = random.randrange(10)
    b = random.randrange(10)
    x.append(a)
    y.append(b)

with open('Python.csv', 'w', newline='') as csvfile:
    fieldnames = ['X', 'Y']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerow({'X': x, 'Y': y})

with open('Python.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        print("Координаты:")
        print("X:" + row['X'])
        print("Y:" + row['Y'])

e = []

for i in range(10):
    a = random.randrange(10)
    b = random.randrange(10)
    e.append(a)

with open('Python.csv', 'w', newline='') as csvfile:
    fieldnames = ['X', 'Y']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerow({'X': e, 'Y': y})

with open('Python.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        print("Новые координаты:")
        print("X:" + row['X'])
        print("Y:" + row['Y'])
